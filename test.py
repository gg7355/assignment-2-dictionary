import subprocess

inp = ["first word", "third word", "pointer", "hello", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"]
outp = ["first word explanation", "third word explanation", "pointer asd", "", ""]
error = ["", "", "", "String not found in dictionary.", "String is longer than 255 bytes."]

for i in range(len(inp)):
    process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=inp[i].encode())
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()
    print("TEST " + str(i+1) + ": " + inp[i])
    if stdout == outp[i] and stderr == error[i]:
        print("RESULT OF TEST " + str(i+1) + ": PASS")
    else:
        print("RESULT OF TEST " + str(i+1) + ": FAIL")

        if stdout != outp[i]:
            print("Need to print in STDOUT: " + outp[i])
            print("Printed in STDOUT: " + stdout)
        if stderr != error[i]:
            print("Need to print in STDERR: " + error[i])
            print("Printed in STDERR: " + stderr)