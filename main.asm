%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define STRING_LENGTH 255

section .bss
	buf: resb STRING_LENGTH

section .rodata
	str_length_err: db "String is longer than 255 bytes.", 0
	str_not_found_err: db "String not found in dictionary.", 0

section .text

global _start

_start:
	xor rax, rax
	xor rdi, rdi
	mov rsi, buf
	mov rdx, STRING_LENGTH
	push rsi
	syscall 
	pop rdi
	cmp rax, STRING_LENGTH
	je .length_err
	mov rsi, pointer
	call find_word
	test rax, rax
	jz .not_found_err
	add rax, 8
	mov rdi, rax
	call string_length
	add rax, 1
	add rdi, rax
	call print_string
	xor rdi, rdi
	call exit
.length_err:
	mov rdi, str_length_err
	jmp .print_err
.not_found_err:
	mov rdi, str_not_found_err
.print_err:
	call print_error
	mov rdi, 1
	call exit
	
