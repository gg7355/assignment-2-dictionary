extern string_equals
global find_word

find_word:
	push rsi
	push rdi
.loop:
	mov rdx, rsi
	cmp rdx, 0
	je .NOT_FOUND
	add rsi, 8
	call string_equals
	cmp rax, 1
	je .FOUND    
	mov rsi, [rsi-8]
	jmp .loop
.NOT_FOUND:
	mov rax, 0
	jmp .end
.FOUND:
	mov rax, rdx
	jmp .end
.end:
	pop rdi
	pop rsi
	ret


