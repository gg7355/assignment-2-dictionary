ASM=nasm
FLAGS=-f elf64
PYTHON=python3

%.o: %.asm
	$(ASM) $(FLAGS) -o $@ $<

program: main.o dict.o lib.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm -f *.o program

.PHONY: test

run: program
	./$^

test: program
	$(PYTHON) test.py




