section .text

%define SYS_WRITE 1
%define FD_STDOUT 1
%define FD_STDERR 2
%define FD_STDIN 0
%define SYS_READ 0
%define NEW_LINE 0xA
%define SPACE 0x20
%define TAB 0x9
%define MINUS '-'
%define PLUS '+'
%define ZERO_DIGIT '0'
%define NINE_DIGIT '9'


global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

; ��������� ��� �������� � ��������� ������� �������
exit:
    mov rax, 60
    syscall

; ��������� ��������� �� ����-��������������� ������, ���������� � �����
string_length:
    xor  rax, rax
    mov rcx, -1
    repne scasb
    neg rcx
    mov rax, rcx
    sub rax, 2
    ret

; ��������� ��������� �� ����-��������������� ������, ������� � � stdout
print_string:
    push rdi
    call string_length
    pop  rsi
    mov  rdx, rax 
    mov  rax, SYS_WRITE
    mov  rdi, FD_STDOUT
    syscall
    ret                

; ��������� ��� ������� � ������� ��� � stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYS_WRITE
    mov rdi, FD_STDOUT
    syscall
    pop rdi
    ret

; ��������� ������ (������� ������ � ����� 0xA)
print_newline:
    mov rdi, NEW_LINE
    jmp print_char


; ������� ����������� 8-�������� ����� � ���������� �������
print_uint:   
    mov rsi, rsp
    mov r8, 10
    sub rsp, 32
    dec rsi
    mov byte [rsi], 0  
    mov rax, rdi
.loop:
    xor rdx, rdx
    div r8
    add rdx, ZERO_DIGIT
    dec rsi
    mov byte[rsi], dl
    test rax, rax
    jne .loop

    mov rdi, rsi
    call print_string
    add rsp, 32
    ret

; ������� �������� 8-�������� ����� � ���������� �������
print_int:
    cmp rdi, 0
    jge .print
    push rdi
    mov rdi, MINUS
    call print_char
    pop rdi
    neg rdi
.print:
    jmp print_uint

; ��������� ��� ��������� �� ����-��������������� ������, ���������� 1 ���� ��� �����, 0 �����
string_equals:
    xor  rax, rax
.loop:
    mov r8B, byte [rdi+rax]  
    cmp r8B, byte [rsi+rax]
    jne   .end0
    cmp byte[rdi+rax], 0
    je    .end1
    inc  rax
    jmp .loop 
.end1:
    mov rax, 1
    ret
.end0:
    mov rax, 0
    ret                            

; ������ ���� ������ �� stdin � ���������� ���. ���������� 0 ���� ��������� ����� ������
read_char:
    mov rax, SYS_READ
    mov rdi, FD_STDIN
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret
    
; ���������: ����� ������ ������, ������ ������
; ������ � ����� ����� �� stdin, ��������� ���������� ������� � ������, .
; ���������� ������� ��� ������ 0x20, ��������� 0x9 � ������� ������ 0xA.
; ��������������� � ���������� 0 ���� ����� ������� ������� ��� ������
; ��� ������ ���������� ����� ������ � rax, ����� ����� � rdx.
; ��� ������� ���������� 0 � rax
; ��� ������� ������ ���������� � ����� ����-����������
read_word:
    push r13
    push r12
    push r14
    xor r14, r14
    mov r12, rdi
    mov r13, rsi
    dec r13
.spaceLoop:
    call read_char
    cmp rax, SPACE
    je .spaceLoop
    cmp rax, TAB
    je .spaceLoop
    cmp rax, NEW_LINE
    je .spaceLoop
.loop:
    cmp rax, 0 
    je .end1
    cmp rax, SPACE
    je .end1
    cmp rax, TAB
    je .end1
    cmp rax, NEW_LINE
    je .end1

    cmp r13, r14
    je .end0
    mov [r12+r14], al
    inc r14
    call read_char 
    jmp .loop 
.end1:
    mov byte[r12+r14], 0
    mov rdx, r14
    mov rax, r12
    jmp .end
.end0:
    xor rax, rax
.end:
    pop r14
    pop r12
    pop r13
    ret
    
; ��������� ��������� �� ������, ��������
; ��������� �� � ������ ����������� �����.
; ���������� � rax: �����, rdx : ��� ����� � ��������
; rdx = 0 ���� ����� ��������� �� �������
parse_uint:
    push r12
    push r13
    push r14
    mov r13, 10
    xor r14, r14
    xor r12, r12
    xor rax, rax
    mov r14b, byte[rdi]
    cmp r14b, ZERO_DIGIT
    jb .end0
    cmp r14b, NINE_DIGIT
    ja .end0
.loop:
    sub r14b, ZERO_DIGIT
    add rax, r14
    inc r12
    mov r14b, byte[rdi+r12]
    cmp r14b, ZERO_DIGIT
    jb .end1
    cmp r14b, NINE_DIGIT
    ja .end1
    mul r13
    jmp .loop
.end0:
    xor rdx, rdx
    jmp .end
.end1:
    mov rdx, r12
.end:   
    pop r14
    pop r13
    pop r12
    ret

; ��������� ��������� �� ������, ��������
; ��������� �� � ������ �������� �����.
; ���� ���� ����, ������� ����� ��� � ������ �� ���������.
; ���������� � rax: �����, rdx : ��� ����� � �������� (������� ����, ���� �� ���) 
; rdx = 0 ���� ����� ��������� �� �������
parse_int:
    xor rax, rax
    mov sil, byte[rdi]
    cmp sil, MINUS
    je .minus
    cmp sil, ZERO_DIGIT
    jb .end0
    cmp sil, NINE_DIGIT
    ja .end0
    jmp parse_uint
.minus:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
.end0:
    xor rdx, rdx
    ret

; ��������� ��������� �� ������, ��������� �� ����� � ����� ������
; �������� ������ � �����
; ���������� ����� ������ ���� ��� ��������� � �����, ����� 0
string_copy:
    xor r8, r8
    xor rax, rax
    xor rcx, rcx
.loop:
    mov cl, byte[rdi+r8]
    cmp rdx, r8
    je .end
    mov byte[rsi+r8], cl
    cmp cl, 0
    je .end1
    inc r8
    jmp .loop
.end1:
    mov rax, r8
.end:
    ret

print_error:
    push rdi
    call string_length
    pop  rsi
    mov  rdx, rax 
    mov  rax, SYS_WRITE
    mov  rdi, FD_STDERR
    syscall
    ret
